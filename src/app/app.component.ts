import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  role: string;
  title = 'JobJob';
  constructor() {
    this.role = "user";
  }

  showHeader() {
    return this.role != "admin"
  }
}
