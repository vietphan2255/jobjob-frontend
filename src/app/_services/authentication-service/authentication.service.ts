import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiUrl} from "../../_helpers/ApiUrl";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    return this.http.post<any>(ApiUrl.ACCOUNT_API + `/authenticate`, {username: username, password: password})
      .pipe(map(account => {
        if (account && account.token) {
          localStorage.setItem('currentUser', JSON.stringify(account));
        }

        return account;
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
  }
}
