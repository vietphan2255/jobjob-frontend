import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiUrl} from "../../_helpers/ApiUrl";
import {a} from "@angular/core/src/render3";

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  getAllAccount() {
    return this.http.get<Account[]>(ApiUrl.ACCOUNT_API);
  }

  getAccountById(idAccount: number) {
    return this.http.get(ApiUrl.ACCOUNT_API + `/` + idAccount);
  }

  registerAccount(account: Account) {
    return this.http.post(ApiUrl.ACCOUNT_API, account);
  }

  updateAccount(account: Account) {
    return this.http.put(ApiUrl.ACCOUNT_API + `/` + account.id, account);
  }

  deleteAccount(idAccount: number) {
    return this.http.delete(ApiUrl.ACCOUNT_API + `/` + idAccount);
  }

}
