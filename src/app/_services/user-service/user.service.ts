import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../../_models/User";
import {ApiUrl} from "../../_helpers/ApiUrl";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getAllUser() {
    return this.http.get<User[]>(ApiUrl.USER_API);
  }

  getUserById(id: number) {
    return this.http.get(ApiUrl.USER_API + `/` + id);
  }

  updateUser(user: User) {
    return this.http.put(ApiUrl.USER_API + `/` + user.id, user);
  }

  deleteUser(id: number) {
    return this.http.delete(ApiUrl.USER_API + `/` + id);
  }
}
