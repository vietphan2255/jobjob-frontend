import { TestBed } from '@angular/core/testing';

import { TopicService } from './service/topic-service/topic.service.ts';

describe('TopicService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TopicService = TestBed.get(TopicService);
    expect(service).toBeTruthy();
  });
});
