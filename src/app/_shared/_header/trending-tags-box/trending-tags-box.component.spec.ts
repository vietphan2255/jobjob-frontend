import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendingTagsBoxComponent } from './trending-tags-box.component';

describe('TrendingTagsBoxComponent', () => {
  let component: TrendingTagsBoxComponent;
  let fixture: ComponentFixture<TrendingTagsBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendingTagsBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendingTagsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
