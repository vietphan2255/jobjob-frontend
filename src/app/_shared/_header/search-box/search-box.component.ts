import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit {
  keyword: string;
  suggestionTags = ["ahihi", "ahuhu"];
  focused: boolean;
  suggestion: boolean;
  constructor() { }

  ngOnInit() {
    this.focused = false;
    this.suggestion = false;
  }

  keyUpHandler() {
    console.log(this.keyword);
  }

  textFocused() {
    this.focused = true;
  }

  textBlured() {
    var that = this;
    setTimeout(function () {
      console.log("may truoc");
      that.focused = false;
    }, 150)
  }

  suggestionFocused() {
    console.log("tao truoc")
    this.suggestion = true;
  }

  suggestionBlured() {
    this.suggestion = false;
  }

  showSuggestion() {
    return ((this.suggestion || this.focused) && this.keyword != "" && this.keyword != null && this.keyword != undefined)
  }
}
