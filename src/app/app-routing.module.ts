import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./_shared/login/login.component";
import {RegisterComponent} from "./_shared/register/register.component";
import {HomeComponent} from "./_shared/home/home.component";
import {HeaderComponent} from "./_shared/_header/header/header.component";
import {FooterComponent} from "./_shared/footer/footer.component";
import {SearchResultComponent} from "./user/search-result/search-result.component";
import {CompanyDetailComponent} from "./company/company-detail/company-detail.component";

const routes: Routes = [
  { path: '', redirectTo: '/companies', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'home', component: HomeComponent},
  { path: 'header', component: HeaderComponent},
  { path: 'footer', component: FooterComponent},
  { path: 'it-jobs', component: SearchResultComponent},
  { path: 'it-jobs/:tag', component: SearchResultComponent},
  { path: 'companies', component: CompanyDetailComponent},
  { path: 'companies/:company', component: CompanyDetailComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class AppRoutingModule {

}
