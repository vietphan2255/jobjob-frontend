import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductOwnerHomeComponent } from './product-owner-home.component';

describe('ProductOwnerHomeComponent', () => {
  let component: ProductOwnerHomeComponent;
  let fixture: ComponentFixture<ProductOwnerHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductOwnerHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductOwnerHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
