import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductOwnerDetailComponent } from './product-owner-detail.component';

describe('ProductOwnerDetailComponent', () => {
  let component: ProductOwnerDetailComponent;
  let fixture: ComponentFixture<ProductOwnerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductOwnerDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductOwnerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
