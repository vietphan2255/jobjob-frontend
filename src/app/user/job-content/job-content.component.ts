import {Component, Input, OnInit} from '@angular/core';
import {Job} from "../../_models/Job";

@Component({
  selector: 'app-job-content',
  templateUrl: './job-content.component.html',
  styleUrls: ['./job-content.component.css']
})
export class JobContentComponent implements OnInit {
  @Input() job: Job;
  constructor() { }

  ngOnInit() {
    console.log(this.job.imageSource)
  }

}
