import { Component, OnInit } from '@angular/core';
import {Job} from "../../_models/Job";

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  jobData =
    {
      id : 1,
      title : "",
      companyName : "",
      imageSource : "https://scontent.fdad3-1.fna.fbcdn.net/v/t1.0-9/10441086_519766731488177_7436812087687675832_n.jpg?_nc_cat=103&_nc_ht=scontent.fdad3-1.fna&oh=27e2aeee4709a905e6b3e299d3a869c9&oe=5C82471D",
      salary : 0,
      detail : "",
      date : 1,
      location : "",
      deadLine : 1,
      tags : []
    };
  job: Job;
  constructor() {
    this.job = new Job(this.jobData);
    console.log("SR");
    console.log(this.job.imageSource);
  }

  ngOnInit() {

  }

}
