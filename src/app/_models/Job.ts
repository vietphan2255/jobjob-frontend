export class Job {
  id: number;
  title: string;
  companyName: string;
  imageSource: string;
  salary: number;
  detail: string;
  date: number;
  location: string;
  deadLine: number;
  link: string;
  tags = [];

  // // @ts-ignore
  // constructor(){
  //   this.id = 1;
  //   this.title = "";
  //   this.companyName = "";
  //   this.imgageSource = "";
  //   this.salary = 0;
  //   this.detail = "";
  //   this.date = 1;
  //   this.location = "";
  //   this.deadLine = 1;
  //   this.tags = [];
  // }

  constructor(data: any){
    this.id           = (data.id == null  || data.id == undefined)?                   1: data.id;
    this.title        = (data.title == null || data.title == undefined)?              "No Title": data.title;
    this.companyName  = (data.companyName == null || data.companyName == undefined)?  "No Name": data.companyName;
    this.imageSource  = (data.imageSource == null || data.imageSource == undefined)?  "/home": data.imageSource;
    this.salary       = (data.salary == null || data.salary == undefined)?            0: data.salary;
    this.detail       = (data.detail == null || data.detail == undefined)?            "No Detail": data.detail;
    this.date         = (data.date == null || data.date == undefined)?                0: data.date;
    this.location     = (data.location == null || data.location == undefined)?        "No Location": data.location;
    this.deadLine     = (data.deadLine == null || data.deadLine == undefined)?        0: data.deadLine;
    this.link         = (data.link == null || data.link == undefined)?                "/home": data.link;
    this.tags         = (data.tags == null || data.tags == undefined)?                []: data.tags;
  }

}
