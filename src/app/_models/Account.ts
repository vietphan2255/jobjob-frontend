export class Account {
  accountId: number;
  username: string;
  password: string;
  email: string;
}
