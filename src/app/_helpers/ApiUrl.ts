export class ApiUrl {
  public static ROOT_API      = 'http://localhost:4200'
  public static USER_API      = 'http://localhost:4200/users';
  public static ACCOUNT_API   = 'http://localhost:4200/accounts';
}
