import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './_shared/home/home.component';
import { HeaderComponent } from './_shared/_header/header/header.component';
import { FooterComponent } from './_shared/footer/footer.component';
import { LoginComponent } from './_shared/login/login.component';
import { AdminHomeComponent } from './admin/admin-home/admin-home.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { CompanyHomeComponent } from './company/company-home/company-home.component';
import { CompanyDetailComponent } from './company/company-detail/company-detail.component';
import { ProductOwnerHomeComponent } from './product_owner/product-owner-home/product-owner-home.component';
import { ProductOwnerDetailComponent } from './product_owner/product-owner-detail/product-owner-detail.component';
import { UserHomeComponent } from './user/user-home/user-home.component';
import { AppRoutingModule } from './app-routing.module';
import { RegisterComponent } from './_shared/register/register.component';
import { AlertComponent } from './_directives/alert/alert.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AlertService} from "./_services/alert-service/alert.service";
import {AuthenticationService} from "./_services/authentication-service/authentication.service";
import {UserService} from "./_services/user-service/user.service";
import {AccountService} from "./_services/account-service/account.service";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { AdminLayoutComponent } from './admin/admin-layout/admin-layout.component';
import {BsDropdownModule, ModalModule, TooltipModule} from "ngx-bootstrap";
import { SearchResultComponent } from './user/search-result/search-result.component';
import { SearchBoxComponent } from './_shared/_header/search-box/search-box.component';
import { TrendingTagsBoxComponent } from './_shared/_header/trending-tags-box/trending-tags-box.component';
import { JobContentComponent } from './user/job-content/job-content.component';
import { SearchCompanyComponent } from './_shared/_header/search-company/search-company.component';
import { CompanyOverviewComponent } from './company/company-overview/company-overview.component';
import {NgbModule, NgbTab, NgbTabsetModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    AdminHomeComponent,
    UserDetailComponent,
    CompanyHomeComponent,
    CompanyDetailComponent,
    ProductOwnerHomeComponent,
    ProductOwnerDetailComponent,
    UserHomeComponent,
    RegisterComponent,
    AlertComponent,
    AdminLayoutComponent,
    SearchResultComponent,
    SearchBoxComponent,
    TrendingTagsBoxComponent,
    JobContentComponent,
    SearchCompanyComponent,
    CompanyOverviewComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [
    AlertService,
    AuthenticationService,
    UserService,
    AccountService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
